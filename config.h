/* See LICENSE file for copyright and license details. */

/* for XF86 Media Keys */
#include <X11/XF86keysym.h>

/* appearance */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int gappx     = 15;        /* gaps between windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int vertpad            = 10;       /* vertical padding of bar */
static const int sidepad            = 10;       /* horizontal padding of bar */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "Liberation Mono:size=26:antialias=true:autohint=true","JoyPixels:size=26:antialias=true:autohint=true"};
static const char dmenufont[]       = "Liberation Mono:size=25";

static const char col_gray1[]         = "#282a36";
static const char col_gray2[]         = "#282a36"; /* border color unfocused windows */
static const char col_gray3[]         = "#96b5b4";
static const char col_gray4[]         = "#d7d7d7";
static const char col_cyan[]          = "#924441"; /* border color focused windows and tags */
static const unsigned int baralpha = 0xd0;
static const unsigned int borderalpha = OPAQUE;
static const char *colors[][3]      = {
  /*               fg         bg         border   */
  [SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
  [SchemeSel]  = { col_gray4, col_cyan,  col_cyan  },
};
static const unsigned int alphas[][3]      = {
  /*               fg      bg        border     */
  [SchemeNorm] = { OPAQUE, baralpha, borderalpha },
  [SchemeSel]  = { OPAQUE, baralpha, borderalpha },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
static const char *tagsalt[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
  /* xprop(1):
   *	WM_CLASS(STRING) = instance, class
   *	WM_NAME(STRING) = title
   */
  /* class      instance    title       tags mask     isfloating   isterminal noswallow monitor */
  { "Gimp",     NULL,       NULL,       0,            1,           0,         0,        -1 },
  { "Firefox",  NULL,       NULL,       1 << 8,       0,           0,         0,        -1 },
  { "st",       NULL,       NULL,       0,            0,           1,         1,        -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

#include "fibonacci.c"

static const Layout layouts[] = {
  /* symbol     arrange function */
  { "[tile]",      tile },    /* first entry is default */
  { "[flt]",      NULL },    /* no layout function means floating behavior */
  { "[Mon]",      monocle },
  { "[Fib]",      spiral },
  { "[Dwin]",      dwindle },
  { "|M|",      centeredmaster },
  { ">M>",      centeredfloatingmaster },
  { NULL,    NULL },
};

/* key definitions */
#define MODKEY Mod4Mask
#define ALTKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/zsh", "-c", cmd, NULL } }



/* audio */
static const char *volup[]         = { "pamixer","-i","10",  NULL };
static const char *voldown[]       = { "pamixer","-d","10",  NULL };
static const char *voltoggle[]     = { "pamixer","-t", NULL };
static const char *volplay[]       = { "mpc","toggle", NULL };
/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenu[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };
static const char *terminal[]  = { "st", NULL };
static const char scratchpadname[] = "scratchpad";
static const char *scratchpad[] = {"st","-t", "scratchpad","-g","60x20",NULL};
static const char * filemanager[] = {"st","-e","lfrun"};
static const char * mail[] = {"st","-e","neomutt"};
static const char *update[] = {"st","-e","pikaur","-Syu","--noconfirm",NULL};
static const char * sink[] = {"sh","-c","~/.config/myscripts/sink_circle.sh",NULL};
static const char * mount[] = {"sh","-c","~/.config/myscripts/mount.sh",NULL};
static const char * umount[] = {"sh","-c","~/.config/myscripts/umount.sh",NULL};
static const char * brightnessup[] = {"sh","-c","~/.config/myscripts/brightness","i",NULL};
static const char * brightnessdown[] = {"sh","-c","~/.config/myscripts/brightness","d",NULL};
static const char * screenshotPart[] = {"sleep","0.2;","scrot","-s", "~/Pictures/Screenshots/",NULL};
static const char * screenshotFull[] = {"sleep","0.2;","scrot","~/Pictures/Screenshots/",NULL};
static const char* sink_select[] = {"zsh","-c","~/.config/myscripts/select_sink.sh",NULL};
static const char * clipmenu[] = {"sh","-c","clipmenu",NULL};
static const char * browser[] = {"sh","-c","firefox",NULL};
static const char * detectscreens[] = {"xrandr","--output","DVI-D-0","--mode","3840x2160","--rate","60","--output","HDMI-0","--mode","3840x2160","--rate","60",NULL};
static const char * shutdown[] = {"sh","-c","poweroff",NULL};
static const char * download_file[] = {"zsh","-c","~/run_deepbrid.sh",NULL};
static const char * bg[] = {"sh","-c","/usr/bin/ls -d ~/Pictures/Wallpapers/* | shuf -n 1 | xargs xwallpaper --maximize",NULL};

static Key keys[] = {
  /* modifier                     key        function        argument */
  { 0,                            XF86XK_AudioRaiseVolume,  spawn,          {.v = volup } },
  { 0,                            XF86XK_AudioLowerVolume,  spawn,          {.v = voldown } },
  { 0,                            XF86XK_AudioMute,         spawn,          {.v = voltoggle } },
  { 0,                            XF86XK_AudioPlay,         spawn,          {.v = volplay } },
  { 0,                            XF86XK_Explorer,          spawn,          {.v = browser } },
  { 0,                            XF86XK_Mail,              spawn,          {.v = mail }},
  { 0,                            XF86XK_HomePage,          spawn,          {.v = browser}},
  { MODKEY,                       XK_y,                     spawn,          {.v = dmenu }},
  { MODKEY|ShiftMask,             XK_f,      togglefullscr,  {0} },
  { MODKEY|ShiftMask,             XK_a,	                  spawn,	  {.v = filemanager}},
  { MODKEY,	                      XK_dead_circumflex,       togglescratch,  {.v=scratchpad}},
  { MODKEY,		                    XK_x,      		  spawn,	  {.v = clipmenu}},
  { MODKEY|ShiftMask,		          XK_s,      		  togglesticky,	  {0}},
  { MODKEY,                       XK_s,      swapfocus,      {.i = -1 } },
  { MODKEY|ShiftMask,		          XK_End,    		  spawn,	  {.v = shutdown}},
  { MODKEY|ShiftMask,	       	    XK_w,      		  spawn,	  {.v= browser}},
  { MODKEY,             	        XK_Return, 		  spawn,          {.v = terminal}},
  { MODKEY,		                    XK_F12,    		  spawn,          {.v = sink}},
  { MODKEY|ShiftMask,	            XK_m,     		  spawn,          {.v = mount}},
  { MODKEY|ShiftMask,	            XK_u,     		  spawn,          {.v = umount}},
  { MODKEY|ShiftMask,	            XK_F6,     		  spawn,          {.v = brightnessup}},
  { MODKEY|ShiftMask,	            XK_F5,     		  spawn,          {.v = brightnessdown}},
  { MODKEY|ShiftMask,	            XK_F11,    		  spawn,          {.v = voldown}},
  { MODKEY|ShiftMask,	            XK_F10,    		  spawn,          {.v = voltoggle}},
  { MODKEY|ShiftMask,	            XK_F12,    		  spawn,          {.v = volup}},
  { MODKEY,		       	            XK_F10,	         	  spawn,          {.v = bg}},
  { MODKEY,		    	              XK_F11,	   		  spawn,          {.v = detectscreens}},
  { 0,		       	                XK_Print,  		  spawn,          {.v = screenshotFull}},
  { MODKEY,		                    XK_Print,  		  spawn,          {.v = screenshotPart}},
  { MODKEY|ShiftMask,	            XK_u,      		  spawn,          {.v = update}},
  { ALTKEY,	            XK_y,      		  spawn,          {.v = sink_select}},
  { MODKEY|ShiftMask,	            XK_q,		          quit,	          {0}},
  { MODKEY,                       XK_b,     		  togglebar,      {0} },
  { MODKEY,                       XK_j,      		  focusstack,     {.i = +1 } },
  { MODKEY,                       XK_k,      		  focusstack,     {.i = -1 } },
  { MODKEY,                       XK_i,      		  incnmaster,     {.i = +1 } },
  { MODKEY,                       XK_d,      		  incnmaster,     {.i = -1 } },
  { MODKEY,                       XK_h,      		  setmfact,       {.f = -0.05} },
  { MODKEY,                       XK_l,      		  setmfact,       {.f = +0.05} },
  { MODKEY|ShiftMask,             XK_Return, 		  zoom,           {0} },
  { MODKEY|ShiftMask,             XK_d, 		  spawn,           {.v=download_file} },
  { MODKEY|ShiftMask,             XK_n, 			  togglealttag,   {0} },
  { MODKEY,                       XK_Tab,    		  view,           {0} },
  { MODKEY,	                      XK_c,       		  killclient,     {0} },
  { MODKEY|ControlMask,           XK_t,      		  setlayout,      {.v = &layouts[0]} }, //tile
  { MODKEY|ControlMask,           XK_h,      		  movefloating,      {.i = '0'} }, //tile
  { MODKEY|ControlMask,           XK_j,      		  movefloating,      {.i = '1'} }, //tile
  { MODKEY|ControlMask,           XK_k,      		  movefloating,      {.i = '2'} }, //tile
  { MODKEY|ControlMask,           XK_l,      		  movefloating,      {.i = '3'} }, //tile
  { MODKEY|ControlMask,           XK_h,      		  setlayout,      {.v = &layouts[1]} }, //floating
  { MODKEY|ControlMask,           XK_m,      		  setlayout,      {.v = &layouts[2]} }, //monocle
  { MODKEY|ControlMask,           XK_f,      		  setlayout,      {.v = &layouts[3]} }, //spiral
  { MODKEY|ControlMask,           XK_g,      		  setlayout,      {.v = &layouts[4]} }, //dwindle
  { MODKEY|ControlMask,           XK_z,      		  setlayout,      {.v = &layouts[5]} }, //centeredmaster
  { MODKEY|ControlMask,           XK_u,      		  setlayout,      {.v = &layouts[6]} }, //centeredmasterflt
  { MODKEY,                       XK_space,  		  setlayout,      {0} },
  { MODKEY|ShiftMask,             XK_space, 		  togglefloating, {0} },
  { MODKEY,                       XK_KP_Divide,      		  shiftview,      {.i = +1 } },
  { MODKEY,                       XK_KP_Multiply,      		  shiftview,      {.i = -1 } },
  { MODKEY,                       XK_0,      		  view,           {.ui = ~0 } },
  { MODKEY|ShiftMask,             XK_0,     		  tag,            {.ui = ~0 } },
  { MODKEY,                       XK_comma,  		  focusmon,       {.i = -1 } },
  { MODKEY,                       XK_period, 		  focusmon,       {.i = +1 } },
  { MODKEY|ShiftMask,             XK_comma,		  tagmon,         {.i = -1 } },
  { MODKEY|ShiftMask,             XK_period,		  tagmon,         {.i = +1 } },
  { MODKEY|ShiftMask,             XK_minus,		  setgaps,        {.i = -1 } },
  { MODKEY|ShiftMask,             XK_plus, 		  setgaps,        {.i = +1 } },
  { MODKEY|ShiftMask,             XK_equal,		  setgaps,        {.i = 0  } },
  TAGKEYS(                        XK_1,                      0)
    TAGKEYS(                        XK_2,                      1)
    TAGKEYS(                        XK_3,                      2)
    TAGKEYS(                        XK_4,                      3)
    TAGKEYS(                        XK_5,                      4)
    TAGKEYS(                        XK_6,                      5)
    TAGKEYS(                        XK_7,                      6)
    TAGKEYS(                        XK_8,                      7)
    TAGKEYS(                        XK_9,                      8)
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
  /* click                event mask      button          function        argument */
  { ClkLtSymbol,          0,              Button1,        cyclelayout,      {.i = +1} },
  { ClkLtSymbol,          0,              Button3,        cyclelayout,      {.i = -1} },
  { ClkWinTitle,          0,              Button2,        zoom,           {0} },
  { ClkStatusText,        0,              Button2,        spawn,          {.v = terminal } },
  { ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
  { ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
  { ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
  { ClkTagBar,            0,              Button1,        view,           {0} },
  { ClkTagBar,            0,              Button3,        toggleview,     {0} },
  { ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
  { ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

